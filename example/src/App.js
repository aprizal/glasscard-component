import React from 'react'

import { GlassComponent } from 'glasscard'
import 'glasscard/dist/index.css'

const App = () => {
  return (
    <div>
      <GlassComponent
        title='Your Title Here 😄'
        desc='this is the description section'
      />
    </div>
  )
}

export default App
