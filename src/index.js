import React from 'react'
import styles from './styles.module.css'

export const GlassComponent = ({ title, desc }) => {
  return (
    <div className={styles.container}>
      <h2 className='title'>{title}</h2>
      <p className='desc'>{desc}</p>
    </div>
  )
}
