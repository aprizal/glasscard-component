# glasscard

> a glassmorphism card components for any frontend framework

[![NPM](https://img.shields.io/npm/v/glasscard.svg)](https://www.npmjs.com/package/glasscard) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save glasscard
```

## Usage

```jsx
import React, { Component } from 'react'

import MyComponent from 'glasscard'
import 'glasscard/dist/index.css'

class Example extends Component {
  render() {
    return <MyComponent />
  }
}
```

## License

MIT © [deaaprizal](https://github.com/deaaprizal)
